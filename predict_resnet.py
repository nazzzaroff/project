from argparse import ArgumentParser

import cv2
import numpy as np
import torch
from torchvision import models, transforms
import matplotlib.pyplot as plt

from generator import Clahe

SIZE = 224

transform = transforms.Compose([
            Clahe(),
            transforms.ToPILImage(),
            transforms.Resize((SIZE, SIZE)),
            transforms.ToTensor(),
        ])


class SaveFeatures():
    def __init__(self, m):
        self.hook = m.register_forward_hook(self.hook_fn)
    def hook_fn(self, module, inputs, output):
        self.features = output.cpu().data.numpy()
    def remove(self):
        self.hook.remove()


def hook(module, inputs, output):
    hook.features = output.cpu().data.numpy()


def getCAM(features_conv, weights_fc, class_idx):
    _, nc, h, w = features_conv.shape
    cam = weights_fc[class_idx].dot(features_conv.reshape((nc, h*w)))
    # cam = torch.nn.functional.relu(torch.from_numpy(cam)).numpy()
    cam = cam.reshape(h, w)
    cam -= np.min(cam)
    cam /= np.max(cam)
    return cam

def predict(agrs):
    model = models.resnet18(pretrained=False)
    model.fc = torch.nn.Linear(512, 6)
    model = torch.nn.Sequential(
        torch.nn.BatchNorm2d(3),
        model
    )

    model.load_state_dict(torch.load(args.weights_path))
    model.cuda()
    model.eval()

    image = cv2.imread(args.image_path)
    inputs = transform(image).cuda()

    res = model._modules['1']
    final_layer = res._modules.get('layer4')
    # activated_features = SaveFeatures(final_layer)
    activated_features = final_layer.register_forward_hook(hook)

    out = model(inputs[None, ...])
    preds = out.softmax(dim=1)
    pred = torch.argmax(preds).cpu().detach().numpy()
    preds = preds.cpu().detach().numpy()
    activated_features.remove()

    weights_params = list(res._modules.get('fc').parameters())
    weights = np.squeeze(weights_params[0].cpu().data.numpy())

    heatmap = dict()

    for i in range(6):

        # heatmap[i] = getCAM(activated_features.features, weights, i)
        heatmap[i] = getCAM(hook.features, weights, i)
        cam = cv2.resize(heatmap[i], (image.shape[1], image.shape[0]))
        heatmap[i] = (cam * 255).astype('uint8')
        heatmap[i] = cv2.applyColorMap(heatmap[i].astype('uint8'), cv2.COLORMAP_JET)
        heatmap[i][cam < .5] = 0
        area =  np.count_nonzero(cam>.5) / (image.shape[0] * image.shape[1]) * preds[:, i][0]
        plt.text(10, 60 + 60 * i,
                f'class {i+1}: Area: {area * 100:.2f} Probability: {preds[:, i][0] * 100:.2f}',
                fontsize=6)

    image = (heatmap[int(pred)] * .5 + image).clip(0, 255).astype('uint8')

    plt.imshow(image[...,::-1])
    plt.show()

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-w_p', '--weights_path', type=str,
                        default='checkpoint/resnetTrain.pth',
                        # default='checkpoint/resnet.pth',
                        help='path to model weights')
    parser.add_argument('-p', '--image_path', type=str,
                        default='ML/1/Test/052.JPG',
                        help='path to image or directory with image')
    args = parser.parse_args()
    predict(args)
