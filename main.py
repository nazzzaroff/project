from argparse import ArgumentParser

import torch
from torchvision import models

from train import train

def main(args):
    # model = models.squeezenet1_0(pretrained=False)
    # model.classifier[1] = torch.nn.Conv2d(512, 6, kernel_size=(1,1), stride=(1,1))
    # model.num_classes = 6

    model = models.resnet18(pretrained=True)
    model.fc = torch.nn.Linear(512, 6)
    model = torch.nn.Sequential(
        torch.nn.BatchNorm2d(3),
        model
    )

    model.cuda()

    optim = torch.optim.Adam(model.parameters(), lr=1e-3)
    criterion = torch.nn.CrossEntropyLoss()

    train(model, optim, criterion, args.epochs, args.batch_size, args.crop_count)

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-e', '--epochs', type=int)
    parser.add_argument('-b', '--batch_size', type=int)
    parser.add_argument('-c', '--crop_count', type=int)
    args = parser.parse_args()
    main(args)
